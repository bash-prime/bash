FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > bash.log'

COPY bash .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' bash
RUN bash ./docker.sh

RUN rm --force --recursive bash
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD bash
